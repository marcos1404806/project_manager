const express= require('express');

function create (req, res, next) {
    res.send('Addresses create');
}

function list (req, res, next) {
    res.send('Addresses list');
  }

function index (req, res, next) {
    res.send('Addresses index');
}

function replace (req, res, next) {
    res.send('Addresses replace');
}

function update(req, res, next) {
    res.send('Addresses update');
}

function destroy (req, res, next) {
    res.send('Addresses destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};