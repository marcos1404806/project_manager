const express= require('express');

function create (req, res, next) {
    res.send('Dashboards create');
}

function list (req, res, next) {
    res.send('Dashboards list');
  }

function index (req, res, next) {
    res.send('Dashboards index');
}

function replace (req, res, next) {
    res.send('Dashboards replace');
}

function update(req, res, next) {
    res.send('Dashboards update');
}

function destroy (req, res, next) {
    res.send('Dashboards destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};