const express= require('express');

function create (req, res, next) {
    res.send('Rol Types create');
}

function list (req, res, next) {
    res.send('Rol Types list');
  }

function index (req, res, next) {
    res.send('Rol Types index');
}

function replace (req, res, next) {
    res.send('Rol Types replace');
}

function update(req, res, next) {
    res.send('Rol Types update');
}

function destroy (req, res, next) {
    res.send('Rol Types destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};