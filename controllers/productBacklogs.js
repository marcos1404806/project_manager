const express= require('express');

function create (req, res, next) {
    res.send('ProductBacklogs create');
}

function list (req, res, next) {
    res.send('ProductBacklogs list');
  }

function index (req, res, next) {
    res.send('ProductBacklogs index');
}

function replace (req, res, next) {
    res.send('ProductBacklogs replace');
}

function update(req, res, next) {
    res.send('ProductBacklogs update');
}

function destroy (req, res, next) {
    res.send('ProductBacklogs destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};