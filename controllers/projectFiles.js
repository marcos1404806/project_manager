const express= require('express');

function create (req, res, next) {
    res.send('Project Files create');
}

function list (req, res, next) {
    res.send('Project Files list');
  }

function index (req, res, next) {
    res.send('Project Files index');
}

function replace (req, res, next) {
    res.send('Project Files replace');
}

function update(req, res, next) {
    res.send('Project Files update');
}

function destroy (req, res, next) {
    res.send('Project Files destroy');
}

module.exports={
    create, list, index, replace, update, destroy
};