# Proyecto Reto 1: Desarrollar una manejador de proyectos



## Diagrama de clases:

![Diagrama de clases](assets/Diagrama de Clases.jpeg)

-Clase User
	-> Es el usuario que ingresa al sistema, puede ser cualquier miembro del equipo con cualquier rol.

-Método Login()
	-> Aqui nos permite que todos los usuarios inicien sesión utilizando al menos tres redes sociales diferentes.

-Enum RoleType
	-> Es el tipo de rol que se tiene en el proyecto

-Enum SocialMedia
	-> Aquí el usuario puede utilizar al menos 3 redes sociales diferentes y son las 3 redes sociales para ingresar al sistema 

-Clase embebida Role
	-> Asegura que cada miembro del equipo tenga un propósito específico en el proyecto y solo pueda realizar ciertas acciones autorizadas.

-Clase Permit
	-> Aquí es cuando el miembro del equipo solo puede realizar ciertas acciones autorizadas

-Clase embebida Adress
	-> Es la dirrección los miembros del equipo.

-Clase embebida Skill
	-> Son las habilidades del miembro del equipo de desarrollo
-Clase Developer
	-> La empresa necesita tener información detallada sobre cada miembro del equipo de desarrollo y nos permite llevar un registro completo de los datos personales.

-Clase ProjectFile
	->  La empresa necesita llevar un registro o expediente para cada proyecto de desarrollo y así mantener un registro organizado y completo de cada proyecto de desarrollo

-Clase UserStory
	-> En esta clase las historias tienen estos datos: 
		Narrativa de la historia: rol, caracteristica, beneficio, prioridad y tiempo estimado.
		Criterios de aceptación: contexto, evento y resultados.
	

-Enum Level
	-> Nos dice que la clase Level se clasifica een tres niveles 					diferentes según su experiencia y las capacidad que tiene un usuario en habilidades especificas. 

-Clase ReleaseBacklog
	-> Será la posible entrega o entrega de una lista de funcionalidades o requisitos del proyecto funcional al Product Owner, aquí la clase se organiza en tres columnas principales: Product Backlog, Release Backlog y Sprint Backlog. 

-Clase Dashboard
	-> Es el tablero donde podemos ver todo el desarrollo del proyecto visualmente.

-Clase ProductBacklog
	-> Lista de todas las funcionalidades, mejoras, requisitos y demás elementos necesarios para el desarrollo del proyecto.

-Clase SprintBacklog
	-> Sprint Backlog: Es la lista de funcionalidades y requisitos seleccionados del Product Backlog para ser completadas durante un sprint.

- Clase evento
	-> Describe la tarea o tareas que serán parte del UserStory.

-Clase result
	-> Describe los resultados que serán parte del UserStory.
	
## Diagrama de interacción:
![Diagrama de Interacción](assets/Diagrama_de_Interacción.png)

- Fujo (user -> sistema -> Rol): 
	-> Todos los usuario del sistema deben poder iniciar sesión en el sistema por medio de mínimo tres redes sociales.
	-> Cada usuario cumple un rol dentro de un proyecto con diferentes permisos.
- Fujo (sistema -> tarjetas):
	-> Explica el flujo de interacción entre los objetos con el dasboard y como dependiendo del rol se pueden hacer distintas actividades.

## Imagen de docker (Dockerhub):
https://hub.docker.com/repository/docker/marcosaguilar10/project_manager/general

## Authors
1. America Guadalupe Martínez Cano - 348810
2. Fátima Monserrath Duarte Pérez - 353324
3. Marcos Alfredo Aguilar Mata - 353223 
